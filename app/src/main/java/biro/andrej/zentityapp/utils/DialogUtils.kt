package biro.andrej.zentityapp.utils

import android.content.Context
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog

object DialogUtils {

	fun createAlertDialog(
		context: Context,
		@StringRes title: Int,
		message: String,
		@StringRes positiveButtonTitle: Int,
		@StringRes negativeButtonTitle: Int,
		cancelable: Boolean = false,
		positiveButtonAction: (() -> Unit)? = null,
		negativeButtonAction: (() -> Unit)? = null
	): AlertDialog {
		val dialog = AlertDialog.Builder(context)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton(positiveButtonTitle) { _, _ -> positiveButtonAction?.invoke() }
				.setNegativeButton(negativeButtonTitle) { dialog, _ ->
					dialog.cancel()
					negativeButtonAction?.invoke()
				}
				.setCancelable(cancelable)
		return dialog.create()
	}

	fun createAlertDialog(context: Context,
						  @StringRes title: Int,
						  @StringRes message: Int,
						  @StringRes positiveButtonTitle: Int,
						  @StringRes negativeButtonTitle: Int,
						  cancelable: Boolean = false,
						  positiveButtonAction: (() -> Unit)? = null,
						  negativeButtonAction: (() -> Unit)? = null): AlertDialog {
		val messageString = context.getString(message)
		return createAlertDialog(context, title, messageString, positiveButtonTitle, negativeButtonTitle,
				cancelable, positiveButtonAction, negativeButtonAction)
	}

	fun createOneButtonAlertDialog(
			context: Context,
			@StringRes title: Int,
			@StringRes message: Int,
			@StringRes buttonTitle: Int,
			cancelable: Boolean = false,
			theme: Int,
			buttonAction: (() -> Unit)? = null
	): AlertDialog {
		val messageString = context.getString(message)
		return createOneButtonAlertDialog(context, title, messageString, buttonTitle, cancelable, theme, buttonAction)
	}

	fun createOneButtonAlertDialog(
			context: Context,
			@StringRes title: Int,
			message: String?,
			@StringRes buttonTitle: Int,
			cancelable: Boolean = false,
			theme: Int,
			buttonAction: (() -> Unit)? = null
	): AlertDialog {
		val dialog = AlertDialog.Builder(context, theme)
				.setTitle(title)
				.setMessage(message)
				.setNegativeButton(buttonTitle) { dialog, _ ->
					dialog.cancel()
					buttonAction?.invoke()
				}
				.setCancelable(cancelable)
		return dialog.create()
	}
}