package biro.andrej.zentityapp.repository

import biro.andrej.zentityapp.Result
import biro.andrej.zentityapp.data.Book

interface BookRepository {
    suspend fun getBooks(): Result<List<Book>, Exception>
}