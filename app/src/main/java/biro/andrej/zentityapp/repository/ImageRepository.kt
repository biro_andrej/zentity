package biro.andrej.zentityapp.repository

import android.graphics.Bitmap

interface ImageRepository {
    fun getImage(url: String): Bitmap?
}