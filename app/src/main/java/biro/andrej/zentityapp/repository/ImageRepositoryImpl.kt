package biro.andrej.zentityapp.repository

import android.graphics.Bitmap
import biro.andrej.zentityapp.networking.NetworkManager

class ImageRepositoryImpl(private val networkManager: NetworkManager) : ImageRepository{

    private val cache = HashMap<String, Bitmap?>()

    override fun getImage(url: String): Bitmap? {
        var cachedImage = cache[url]
        if (cachedImage == null) {
            cachedImage = networkManager.call(url, NetworkManager.NetworkType.IMAGE)
            cache[url] = cachedImage
        }

        return cachedImage
    }
}