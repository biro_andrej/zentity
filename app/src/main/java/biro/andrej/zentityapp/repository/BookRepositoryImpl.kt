package biro.andrej.zentityapp.repository

import biro.andrej.zentityapp.Failure
import biro.andrej.zentityapp.Result
import biro.andrej.zentityapp.Success
import biro.andrej.zentityapp.data.Book
import biro.andrej.zentityapp.data.Reader
import biro.andrej.zentityapp.networking.NetworkManager
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule

class BookRepositoryImpl(private val networkManager: NetworkManager) : BookRepository {

    private companion object {
        private const val REPO_URL = "http://www.lukaspetrik.cz/filemanager/tmp/reader/data.xml"
    }

    private val kotlinXmlMapper = XmlMapper(JacksonXmlModule().apply {
        setDefaultUseWrapper(false)
    }).registerKotlinModule()
        .configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

    override suspend fun getBooks(): Result<List<Book>, Exception> = try {
        networkManager.call<String>(REPO_URL, NetworkManager.NetworkType.STRING).run {
            val books = kotlinXmlMapper.readValue(this, Reader::class.java).books
            Success(books)
        }
    } catch (ex: Exception) {
        Failure(ex)
    }
}