package biro.andrej.zentityapp.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.core.content.getSystemService
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import biro.andrej.zentityapp.utils.InstanceStateProvider
import dagger.android.support.AndroidSupportInjection


abstract class BaseFragment<VIEW_MODEL> : Fragment() where VIEW_MODEL : ViewModel {

    private companion object {
        private const val STATE_EXTRA_KEY = "_state"
    }

    private val state = Bundle()
    private var isFirstTimeCreated = true
    protected var viewModel: VIEW_MODEL? = null
        private set

    private val inputMethodManager by lazy {
        activity?.getSystemService<InputMethodManager>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)

        savedInstanceState?.let {
            state.putAll(savedInstanceState.getBundle(STATE_EXTRA_KEY))
            isFirstTimeCreated = false
        }

        super.onCreate(savedInstanceState)
        viewModel = createViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayoutRes(), null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        alterView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBundle(STATE_EXTRA_KEY, state)
        super.onSaveInstanceState(outState)
    }

    protected abstract fun alterView()
    protected abstract fun createViewModel(): VIEW_MODEL?

    @LayoutRes
    protected abstract fun getLayoutRes(): Int

    protected fun <T> instanceState() = InstanceStateProvider.Nullable<T>(state)

    protected fun createViewModelWithFactory(
        factory: ViewModelProvider.Factory,
        viewModelClass: Class<VIEW_MODEL>
    ): VIEW_MODEL {
        return ViewModelProviders.of(this, factory).get(viewModelClass)
    }

    protected fun hideKeyboard() {
        activity?.currentFocus?.run {
            inputMethodManager?.hideSoftInputFromWindow(windowToken, 0)
        }
    }
}