package biro.andrej.zentityapp.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import biro.andrej.zentityapp.R
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class BaseActivity<FRAGMENT> : AppCompatActivity(
    R.layout.base_activity_layout
), HasSupportFragmentInjector where FRAGMENT : Fragment {

    protected val TAG = this.javaClass.name

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    protected lateinit var fragment: FRAGMENT

    /*-------------------------*/
    /*   OVERRIDDEN METHODS    */
    /*-------------------------*/

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragment = createMainFragment()
            fragmentTransaction.add(R.id.container, fragment, TAG)
            fragmentTransaction.commit()

        } else {
            fragment = supportFragmentManager.findFragmentByTag(TAG) as FRAGMENT
        }
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return fragmentDispatchingAndroidInjector
    }

    protected abstract fun createMainFragment(): FRAGMENT
}