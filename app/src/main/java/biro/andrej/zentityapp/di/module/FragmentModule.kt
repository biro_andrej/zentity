package biro.andrej.zentityapp.di.module

import biro.andrej.zentityapp.ui.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    internal abstract fun mainFragment(): MainFragment
}