package biro.andrej.zentityapp.di.module


import biro.andrej.zentityapp.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    internal abstract fun bindMainActivity(): MainActivity
}