package biro.andrej.zentityapp.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import biro.andrej.cleverlanceapp.ViewModelFactory
import biro.andrej.cleverlanceapp.ViewModelKey
import biro.andrej.zentityapp.ui.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun startViewModel(mainViewModel: MainViewModel): ViewModel
}