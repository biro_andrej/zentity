package biro.andrej.zentityapp.di

import biro.andrej.zentityapp.App
import biro.andrej.zentityapp.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [(AndroidSupportInjectionModule::class), (AppModule::class), (ActivityModule::class),
        (FragmentModule::class), (ViewModelModule::class), (RepositoryModule::class)]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}