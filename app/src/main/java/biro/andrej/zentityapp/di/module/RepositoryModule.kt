package biro.andrej.zentityapp.di.module

import biro.andrej.zentityapp.networking.NetworkManager
import biro.andrej.zentityapp.repository.BookRepository
import biro.andrej.zentityapp.repository.BookRepositoryImpl
import biro.andrej.zentityapp.repository.ImageRepository
import biro.andrej.zentityapp.repository.ImageRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    internal fun provideBookRepository(networkManager: NetworkManager): BookRepository =
        BookRepositoryImpl(networkManager)

    @Provides
    internal fun provideDownloadImageRepository(networkManager: NetworkManager): ImageRepository =
        ImageRepositoryImpl(networkManager)
}