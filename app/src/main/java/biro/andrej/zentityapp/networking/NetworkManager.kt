package biro.andrej.zentityapp.networking

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import javax.inject.Inject

class NetworkManager @Inject constructor() {

    fun <T> call(url: String, networkType: NetworkType): T? {
        val connection: HttpURLConnection = URL(url).openConnection() as HttpURLConnection
        var result: T? = null
        try {
            connection.inputStream.use {
                result = when (networkType) {
                    NetworkType.STRING -> readString(BufferedReader(InputStreamReader(it))) as T
                    NetworkType.IMAGE -> readBitmap(it) as T
                }
                it.close()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }

        connection.disconnect()
        return result
    }

    private fun readString(inputStream: BufferedReader): String {
        val response = StringBuffer()
        var inputLine = inputStream.readLine()
        while (inputLine != null) {
            response.append(inputLine)
            inputLine = inputStream.readLine()
        }
        return response.toString()
    }

    private fun readBitmap(inputStream: InputStream): Bitmap {
        return BitmapFactory.decodeStream(inputStream)
    }

    enum class NetworkType {
        STRING, IMAGE
    }
}