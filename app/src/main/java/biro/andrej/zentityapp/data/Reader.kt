package biro.andrej.zentityapp.data

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonRootName

@JsonRootName("ZREADER")
data class Reader(
    @set:JsonProperty("BOOK")
    var books: List<Book> = ArrayList()
)