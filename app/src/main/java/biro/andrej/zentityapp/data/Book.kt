package biro.andrej.zentityapp.data

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonRootName

@JsonRootName("BOOK")
data class Book(
    @set:JsonProperty("ID")
    var id: String? = null,

    @set:JsonProperty("TITLE")
    var title: String? = null,

    @set:JsonProperty("THUMBNAIL")
    var thumbnailUrl: String? = null,

    @set:JsonProperty("NEW")
    var isNew: String? = null,

    @set:JsonProperty("THUMB_EXT")
    var thumbExtension: String? = null,

    @set:JsonProperty("<PDF_EXT")
    var pdfExtension: String? = null
) {

    fun isNew() = "TRUE" == this.isNew
}
