package biro.andrej.zentityapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import biro.andrej.zentityapp.R
import biro.andrej.zentityapp.base.BaseFragment
import biro.andrej.zentityapp.data.Book
import biro.andrej.zentityapp.extensions.observe
import biro.andrej.zentityapp.extensions.toast
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.book__item.view.*
import kotlinx.android.synthetic.main.main__fragment.*
import javax.inject.Inject

class MainFragment : BaseFragment<MainViewModel>() {

    /*-------------------------*/
    /*         FIELDS          */
    /*-------------------------*/

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val bookAdapter by lazy { BookAdapter(viewModel) }
    private val shelfAdapter by lazy { ShelfAdapter() }

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun getLayoutRes(): Int = R.layout.main__fragment

    override fun alterView() {
        val itemCount = resources.getInteger(R.integer.item_count)
        with(book_list) {
            layoutManager =
                GridLayoutManager(requireContext(), itemCount, RecyclerView.VERTICAL, false)
            adapter = bookAdapter
        }

        shelf_list.adapter = shelfAdapter

        viewModel?.let { vm ->
            observe(vm.books) { observeBookState(it) }
            vm.getBooks()
        }
    }

    override fun createViewModel() =
        createViewModelWithFactory(viewModelFactory, MainViewModel::class.java)

    private fun observeBookState(state: MainViewModel.BookState) {
        progress_bar.isVisible = state is MainViewModel.BookState.Loading

        when (state) {
            is MainViewModel.BookState.Success -> {
                bookAdapter.items = state.books
                val itemCount = resources.getInteger(R.integer.item_count)
                val shelfRowCount = state.books.size / itemCount
                val count =
                    if ((state.books.size % itemCount) == 0) shelfRowCount else shelfRowCount + 1
                shelfAdapter.count = count
            }
            MainViewModel.BookState.Error -> context?.toast(R.string.can_not_load_books)
        }
    }

    private class BookAdapter(private val viewModel: MainViewModel?) :
        RecyclerView.Adapter<BookViewHolder>() {
        var items: List<Book>? = null
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.book__item, parent, false)
            return BookViewHolder(view)
        }

        override fun getItemCount() = items?.size ?: 0

        override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
            with(holder.itemView) {
                items?.get(position)?.let { book ->
                    shadow.isVisible = false
                    book_title.text = book.title
                    image_progress.isVisible = true
                    is_new.isVisible = book.isNew()

                    viewModel?.getImage(book) {
                        image_progress.isVisible = false

                        if (it == null) {
                            iv_book.setBackgroundResource(R.drawable.unknown_image)
                        } else {
                            iv_book.setImageBitmap(it)
                        }
                        shadow.isVisible = false
                    }
                }
            }
        }
    }

    class BookViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer

    private class ShelfAdapter : RecyclerView.Adapter<ShelfViewHolder>() {
        var count = 0
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShelfViewHolder {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.shelf__item, parent, false)
            return ShelfViewHolder(view)
        }

        override fun getItemCount() = count

        override fun onBindViewHolder(holder: ShelfViewHolder, position: Int) {
            // empty
        }
    }

    private class ShelfViewHolder(view: View) : RecyclerView.ViewHolder(view)
}