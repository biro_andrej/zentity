package biro.andrej.zentityapp.ui

import android.graphics.Bitmap
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import biro.andrej.zentityapp.data.Book
import biro.andrej.zentityapp.onFailure
import biro.andrej.zentityapp.onSuccess
import biro.andrej.zentityapp.repository.BookRepository
import biro.andrej.zentityapp.repository.ImageRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val bookRepository: BookRepository,
    private val imageRepository: ImageRepository
) : ViewModel() {

    private val _books = MutableLiveData<BookState>()
    val books: LiveData<BookState> = _books

    private val uiHandler by lazy {
        Handler(Looper.getMainLooper())
    }

    fun getBooks() {
        viewModelScope.launch(Dispatchers.Default) {
            _books.postValue(BookState.Loading)

            bookRepository.getBooks().onSuccess {
                _books.postValue(BookState.Success(it))
            }.onFailure {
                _books.postValue(BookState.Error)
            }
        }
    }

    fun getImage(book: Book, onLoaded: (bitmap: Bitmap?) -> Unit) {
        viewModelScope.launch(Dispatchers.Default) {
            book.thumbnailUrl?.let {
                val image = imageRepository.getImage(it)
                uiHandler.post { onLoaded(image) }
            }
        }
    }

    sealed class BookState {
        object Loading : BookState()
        data class Success(val books: List<Book>) : BookState()
        object Error : BookState()
    }
}