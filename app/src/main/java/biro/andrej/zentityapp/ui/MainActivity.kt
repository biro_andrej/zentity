package biro.andrej.zentityapp.ui

import biro.andrej.zentityapp.base.BaseActivity

class MainActivity : BaseActivity<MainFragment>() {

    override fun createMainFragment() = MainFragment.newInstance()
}
